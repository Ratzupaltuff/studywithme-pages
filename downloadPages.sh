#!/bin/bash
curl -L --header "PRIVATE-TOKEN: ${GITSCC_TOKEN}" "https://git.scc.kit.edu/api/v4/projects/30828/jobs/artifacts/main/download?job=pages" --output pages.zip
curl -L --header "PRIVATE-TOKEN: ${GITSCC_TOKEN}" "https://git.scc.kit.edu/api/v4/projects/30827/jobs/artifacts/main/download?job=pages" --output pagesclient.zip
